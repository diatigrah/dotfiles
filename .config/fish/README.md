Fish Shell Configuration
========================

Fish shell configuration contains:

  - Custom two line prompt with full path, time and current git branch and status.
  - Custom paths for Cabal, RVM and local xmonad bin
 
Custom functions for:

  - Custom cd with RVM related changes
  - Custom rvm script
  - Custom prompt_pwd.fish function for full path display on prompts
