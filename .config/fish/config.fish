# Path to your oh-my-fish.
set fish_path $HOME/.oh-my-fish

# Theme
set fish_theme robbyrussell

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-fish/plugins/*)
# Custom plugins may be added to ~/.oh-my-fish/custom/plugins/
# Example format: set fish_plugins autojump bundler

# Path to your custom folder (default path is $FISH/custom)
#set fish_custom $HOME/dotfiles/oh-my-fish

# Load oh-my-fish configuration.
. $fish_path/oh-my-fish.fish


# Fish git prompt
set __fish_git_prompt_showdirtystate 'yes'
set __fish_git_prompt_showstashstate 'yes'
set __fish_git_prompt_showupstream 'yes'
set __fish_git_prompt_color_branch yellow
set __fish_git_prompt_color_suffix blue

# Status Chars
set __fish_git_prompt_char_dirtystate '⚡'
set __fish_git_prompt_char_stagedstate '→'
set __fish_git_prompt_char_stashstate '↩'
set __fish_git_prompt_char_upstream_ahead '↑'
set __fish_git_prompt_char_upstream_behind '↓'

set -g -x AWS_ACCESS_KEY_ID 'AKIAIWI37JWNNULTPECA' 
set -g -x AWS_SECRET_ACCESS_KEY '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'
set -g -x AWS_CREDENTIAL_FILE '/Applications/RDSCli-1.15.001/credential-file'
set -g -x AWS_RDS_HOME '/Applications/RDSCli-1.15.001'
set -g -x JAVA_HOME '/Library/Internet Plug-Ins/JavaAppletPlugin.plugin/Contents/Home'

set PATH /Applications/RDSCli-1.15.001/bin $PATH

# set -xg DOCKER_HOST tcp://192.168.59.103:2375
set -x DOCKER_TLS_VERIFY 1
set -x DOCKER_HOST tcp://192.168.59.103:2376
set -x DOCKER_CERT_PATH /Users/hargitaidavid/.boot2docker/certs/boot2docker-vm

set PATH $HOME/.rbenv/bin $PATH
set PATH $HOME/.rbenv/shims $PATH
rbenv rehash >/dev/null ^&1

set PATH /usr/local/bin /usr/local/sbin $PATH
set PATH ~/.cabal/bin ~/.xmonad/bin $PATH
# set -xg TERM screen-256color

function fish_prompt
  set last_status $status

  set_color green
  printf '┌─┤%s@%s ' (whoami) (hostname|cut -d . -f 1)

  set_color $fish_color_cwd
  printf '%s' (prompt_pwd)

  printf '%s \n' (__fish_git_prompt)

  set_color green
  printf '└─┤'

  set_color cyan
  printf ' %s ' (date "+%H:%M:%S")

  set_color green
  printf '├─ '
  set_color $fish_color_cwd
  printf '$ '

  set_color normal
end

function composer
  command /usr/local/bin/composer.phar $argv
end

function vagrant
  command /usr/bin/vagrant $argv
end

function adb
  command /Users/hargitaidavid/Development/android-sdk-macosx/platform-tools/adb $argv
end

function sp
  command bin/phpspec $argv
end

function bh
  command bin/behat $argv
end

function n98
  command /usr/local/bin/n98-magerun.phar $argv
end

function micsinaltam
  command git log --abbrev-commit --date=short -100 --pretty=format:'%ae %ad %Cred%h%Creset -%C(yellow)%d%Creset %C(bold blue)%s%Creset' | grep "dhargitai@sessiondigital.com" | grep -oE "\d{4}.*"
end

function go
  command git checkout $argv
end
